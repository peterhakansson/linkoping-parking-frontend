FROM python:3.8-alpine

EXPOSE 5000/tcp
WORKDIR /app

# Copy the dependencies file to the working directory
COPY requirements.txt .

# Get libxslt for lxml 
RUN apk add --update --no-cache g++ gcc libxslt-dev

# Install any dependencies
RUN pip install -r requirements.txt


COPY templates/ templates/
COPY app.py .

# Specify the command to run on container start
CMD [ "python", "./app.py" ]